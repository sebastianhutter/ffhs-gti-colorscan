# docker image containing the arm toolchain

FROM debian:stretch-slim

RUN apt-get update \
    && apt-get -y install gcc-arm-none-eabi gdb-arm-none-eabi \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /source