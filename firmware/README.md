# minimal firmware for a working raspberry pi sd card

* fixup.dat - Needed to use 1GB of memory (commit: 93aae13)
* start.elf - GPU firmware, load the other files and start the CPUs (commit: 93aae13)
* bootcode.bin -  First file read by the ROM. Enable SDRAM, and load Boot loader start.elf  (commit: 2304778)
* config.txt - configuration

## weblinks
* [Programming raspberry pi3](https://archive.fosdem.org/2017/schedule/event/programming_rpi3/attachments/slides/1475/export/events/attachments/programming_rpi3/slides/1475/bare_metal_rpi3.pdf)
* [Raspberry PI firmware repository](https://github.com/raspberrypi/firmware/tree/master/boot)
* [Raspberry PI configuration file](https://github.com/raspberrypi/documentation/blob/master/configuration/config-txt/README.md)