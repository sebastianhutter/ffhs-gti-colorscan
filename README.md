# ffhs-pi-assembler

## Development Environment

### Requirements
We use docker and qemu to develop and test our ffhs pi assembler project

- Docker: We use docker to run the gcc-arm tools - https://docs.docker.com/install/
- Make: We use make to execute the build stepts - http://gnuwin32.sourceforge.net/packages/make.htm (for windows)
- Qemu: We use qemu to run the compiled binary in an emulated ARM - https://www.qemu.org/download

### Example
#### run example code
To test the setup you can run `make example`.
This will compile the docker image used to compile source code for arm.
It will then compile the code and start a new qemu instance with the example program running.

##### Qemu
The qemu window will not show any output - to see the output of the program - which is written to the serial console - you need to hit ctrl+alt+3 to switch to the qemu serial console. You should see the program writting the letters 0-7 endlessly.

#### run example code with gdb
We can also run qemu and connect remotely with gdb.
Run `make example-gdb`. After the qemu instance starts we can use the docker container to connect to the remote gdb session.

```
# run gdb 
make debugger
# the gdb console starts
# first we load the debugging symbols from the binary
(gdb) file bin/example
# next we connect to the remote debugger
# ATTENTION: for mac you need to replace 172.17.0.1 with docker.for.mac.host.internal !!!
target remote 172.17.0.1:1234
```

## Weblinks
- http://doppioandante.github.io/2015/07/10/Simple-ARM-programming-on-linux.html
- https://stackoverflow.com/questions/15802748/arm-assembly-using-qemu
- http://people.cs.pitt.edu/~mosse/gdb-note.html
- https://asciich.ch/wordpress/raspberrypi-bare-metal-programmierung-compiler-unter-archlinux-einrichten/
- https://stackoverflow.com/questions/38956680/difference-between-arm-none-eabi-and-arm-linux-gnueabi