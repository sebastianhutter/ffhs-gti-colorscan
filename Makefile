# Makefile

export SRCDIR = src
export BINDIR = bin
export KERNELIMG = kernel.img
export SDVOLUME = /Volumes/boot
export COPS = -Wall -Werror -O2 -nostartfiles -ffreestanding -mfpu=neon-vfpv4 -march=armv7-a -mfloat-abi=softfp -mfpu=vfp

####
# general targets
####

docker-image:
	docker build -t ffhs-pi-assembler .

clean:
	rm -vf $(BINDIR)/* *.o *.elf

prepare: docker-image clean

####
# generate kernel image
# copy firmware and kernel image to sd card
# eject the sd card
# .....
# execute `make prepare` first
####

vectors.o: vectors.s
	docker run --rm -ti -v ${CURDIR}:/source ffhs-pi-assembler \
		arm-none-eabi-as vectors.s -o vectors.o

kernel.o: $(SRCDIR)/$(SRC)
ifndef SRC
	$(error SRC is not set)
endif
	docker run --rm -ti -v ${CURDIR}:/source ffhs-pi-assembler \
		arm-none-eabi-gcc $(COPS) -c $(SRCDIR)/$(SRC) -o kernel.o

functions.o:
	docker run --rm -ti -v ${CURDIR}:/source ffhs-pi-assembler \
		arm-none-eabi-gcc $(COPS) -c $(SRCDIR)/functions.c -o functions.o

kernel.elf: memmap vectors.o kernel.o functions.o
	docker run --rm -ti -v ${CURDIR}:/source ffhs-pi-assembler \
		arm-none-eabi-ld  vectors.o functions.o kernel.o -T memmap -o kernel.elf

img: kernel.elf
	docker run --rm -ti -v ${CURDIR}:/source ffhs-pi-assembler \
		arm-none-eabi-objcopy kernel.elf -O binary bin/$(KERNELIMG)

copy:
	cp -f bin/$(KERNELIMG) $(SDVOLUME)/
	cp -f firmware/* $(SDVOLUME)/
	rm -rf $(SDVOLUME)/._* $(SDVOLUME)/.fseventsd $(SDVOLUME)/.Trashes
	sudo diskutil unmount $(SDVOLUME)/

###
# some helper functions
###
objdump: kernel.elf
	docker run --rm -ti -v ${CURDIR}:/source ffhs-pi-assembler \
		arm-none-eabi-objdump -D kernel.elf


debugger: docker-image
	docker run --rm -ti -v ${CURDIR}:/source ffhs-pi-assembler arm-none-eabi-gdb

####
# A valid sdcard is fat32 formatted and the volume is called 'boot'!!!
####
card: clean img copy

format:
ifndef DISK
	$(error DISK is not set)
endif
	# erase disk and create a 64MB parttition (free space part is necessaryso it doesnt fill up the whole space)
	diskutil partitionDisk $(DISK) MBR FAT32 REPLACEME 64m "Free Space" empty r
	sudo diskutil unmountDisk $(DISK)
	# brew install dosfstools ....
	sudo fatlabel $(DISK)s1 boot
	sudo diskutil mountDisk $(DISK)

####
# simple qemu based example
####

example-as: example/example.s
	docker run --rm -ti -v ${CURDIR}:/source ffhs-pi-assembler arm-none-eabi-as example/example.s -g -o example.o

example-ld: example-as
	docker run --rm -ti -v ${CURDIR}:/source ffhs-pi-assembler arm-none-eabi-ld example.o -o $(BINDIR)/example

example: docker-image example-ld
	qemu-system-arm -M versatilepb -m 128M -kernel bin/example

example-gdb: docker-image example-ld
	qemu-system-arm -M versatilepb -m 128M -singlestep -gdb tcp:0.0.0.0:1234 -kernel bin/example
