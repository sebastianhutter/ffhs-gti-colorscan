/*
    http://www.instructables.com/id/Using-an-RGB-LED-to-Detect-Colours/
    http://wiring.org.co/learning/basics/rgbled.html

    RGB PIN 1 (RED)   to PIN31 (GPIO6)
    RGB PIN 2 (GRND)  to GRND 
    RGB PIN 3 (GREEN) to PIN33 (GPIO13)
    RGB PIN 4 (BLUE)  to PIN35 (GPIO19)

    PCELL PIN 1       to GRND
    PCELL PIN 2       to PIN29 (GPIO5)
*/

#include "functions.h"


int application(void)
{


    // initialize the busy led
    busy_init();

    // initialize the lcd
    lcd_init();

    // initilaize the rgb led
    rgb_init();
    
    // initialize the photocell
    photo_init();

    while(1)
    {
        busy_blink();
        lcd_sendString("RGB!");
        delay(500000);
        rgb_setColor(1,0,0);
        delay(1000000);
        rgb_setColor(0,1,0);
        delay(1000000);
        rgb_setColor(0,0,1);
        delay(1000000);
        rgb_setColor(0,0,0);
        delay(1000000);
        lcd_clearDisplay();

        photo_getPin();

    }
    return 0;
}
