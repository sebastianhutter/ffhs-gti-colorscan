/********************************************************
    some functions for our project
*********************************************************/

/*
    GENERAL
*/

// simple sleep function
void delay(unsigned int t);

/*
    GPIO
*/

// initialize a gpio pin as input or output
void gpio_in(unsigned int gpio);
void gpio_out(unsigned int gpio);

// send HIGH followed by LOW to GPIO
void gpio_blink(unsigned int gpio, unsigned int time);

/*
    BUSY LED
*/
void busy_init();
void busy_blink();

/*
    LCD
*/

// initialize the lcd
void lcd_init();

// send data to the GPIO pins
// function accepts an array 
// data[0] = RS
// data[1] = D7 
// data[2] = D6 
// ...
// data[8] = D0 
void lcd_send(int *data);

// send character to lcd
void lcd_sendChar(char c);

// send string to lcd
void lcd_sendString(char *sentence);

// pulse the EN pin to instruct the lcd to read the data
void lcd_pulse();

// clear the lcd display
void lcd_clearDisplay();

// shift the cursor left or right
// use 'l' for left and 'r' for right
void lcd_shiftCursor(char direction);

// set the cursor position
// line can be 0 or 1
// position can be 0 to 15
void lcd_setCursor(int line, int position);

/*
    RGB and PHOTOCELL 
*/
void rgb_init();

void rgb_setColor(int r, int g, int b);

void photo_init();

void photo_getPin();