#define GPIOREGS       0x3F200000
 
#define GPFSEL0    0
#define GPFSEL1    1
#define GPFSEL2    2
#define GPFSEL3    3
#define GPFSEL4    4
#define GPFSEL5    5
// 6 is reserved
#define GPSET0     7
#define GPSET1     8
// 9 is reserved
#define GPCLR0     10
#define GPCLR1     11
 
//volatile unsigned int* gpioregs;
// https://www.raspberrypi.org/forums/viewtopic.php?f=72&t=206982 
volatile __attribute__((aligned(4))) unsigned int* gpioregs;

void delay(unsigned int t) {
    static volatile unsigned int time;
    for (time = 0; time < t; time++);
}
 
void blink(unsigned int t, unsigned int l) {
    delay(t);
    gpioregs[GPCLR0] = (1 << l);
    delay(t);
    gpioregs[GPSET0] = (1 << l);
}

int main(void)
{
    // just to be safe lets wait for a little while
    delay(1000000);

    // create pointer to the gpioregs;
    gpioregs = (unsigned int*)GPIOREGS;
 
    // lets try to set all pins to output
    // select register 0
    gpioregs[GPFSEL0] &= ~(7 << 0); gpioregs[GPFSEL0] |= (1 << 0);
    gpioregs[GPFSEL0] &= ~(7 << 3); gpioregs[GPFSEL0] |= (1 << 3);
    gpioregs[GPFSEL0] &= ~(7 << 6); gpioregs[GPFSEL0] |= (1 << 6);
    gpioregs[GPFSEL0] &= ~(7 << 9); gpioregs[GPFSEL0] |= (1 << 9);
    gpioregs[GPFSEL0] &= ~(7 << 12); gpioregs[GPFSEL0] |= (1 << 12);
    gpioregs[GPFSEL0] &= ~(7 << 15); gpioregs[GPFSEL0] |= (1 << 15);
    gpioregs[GPFSEL0] &= ~(7 << 18); gpioregs[GPFSEL0] |= (1 << 18);
    gpioregs[GPFSEL0] &= ~(7 << 21); gpioregs[GPFSEL0] |= (1 << 21);
    gpioregs[GPFSEL0] &= ~(7 << 24); gpioregs[GPFSEL0] |= (1 << 24);
    gpioregs[GPFSEL0] &= ~(7 << 27); gpioregs[GPFSEL0] |= (1 << 27);
    // select regiser 1
    gpioregs[GPFSEL1] &= ~(7 << 0); gpioregs[GPFSEL1] |= (1 << 0);
    gpioregs[GPFSEL1] &= ~(7 << 3); gpioregs[GPFSEL1] |= (1 << 3);
    gpioregs[GPFSEL1] &= ~(7 << 6); gpioregs[GPFSEL1] |= (1 << 6);
    gpioregs[GPFSEL1] &= ~(7 << 9); gpioregs[GPFSEL1] |= (1 << 9);
    gpioregs[GPFSEL1] &= ~(7 << 12); gpioregs[GPFSEL1] |= (1 << 12);
    gpioregs[GPFSEL1] &= ~(7 << 15); gpioregs[GPFSEL1] |= (1 << 15);
    gpioregs[GPFSEL1] &= ~(7 << 18); gpioregs[GPFSEL1] |= (1 << 18);
    gpioregs[GPFSEL1] &= ~(7 << 21); gpioregs[GPFSEL1] |= (1 << 21);
    gpioregs[GPFSEL1] &= ~(7 << 24); gpioregs[GPFSEL1] |= (1 << 24);
    gpioregs[GPFSEL1] &= ~(7 << 27); gpioregs[GPFSEL1] |= (1 << 27);
    // select register 2
    gpioregs[GPFSEL2] &= ~(7 << 0); gpioregs[GPFSEL2] |= (1 << 0);
    gpioregs[GPFSEL2] &= ~(7 << 3); gpioregs[GPFSEL2] |= (1 << 3);
    gpioregs[GPFSEL2] &= ~(7 << 6); gpioregs[GPFSEL2] |= (1 << 6);
    gpioregs[GPFSEL2] &= ~(7 << 9); gpioregs[GPFSEL2] |= (1 << 9);
    gpioregs[GPFSEL2] &= ~(7 << 12); gpioregs[GPFSEL2] |= (1 << 12);
    gpioregs[GPFSEL2] &= ~(7 << 15); gpioregs[GPFSEL2] |= (1 << 15);
    gpioregs[GPFSEL2] &= ~(7 << 18); gpioregs[GPFSEL2] |= (1 << 18);
    gpioregs[GPFSEL2] &= ~(7 << 21); gpioregs[GPFSEL2] |= (1 << 21);

    // set all pins to HIGH
    for(int i=0; i<28; i++){
        gpioregs[GPCLR0] = (1 << i);
        gpioregs[GPSET0] = (1 << i);
    }

    while(1)
    {
        blink(500000,22);
        blink(500000,17);
    }
    return 0;
}