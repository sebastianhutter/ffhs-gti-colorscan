#define GPIOREGS       0x20200000
 
#define GPFSEL0    0
#define GPFSEL1    1
#define GPFSEL2    2
#define GPFSEL3    3
#define GPFSEL4    4
#define GPFSEL5    5
// 6 is reserved
#define GPSET0     7
#define GPSET1     8
// 9 is reserved
#define GPCLR0     10
#define GPCLR1     11
 
volatile unsigned int* gpioregs;
 
void delay(unsigned int t) {
    static volatile unsigned int time;
    for (time = 0; time < t; time++);
}
 
void blink(unsigned int t, unsigned int l) {
    delay(t);
    gpioregs[GPCLR0] = (1 << l);
    delay(t);
    gpioregs[GPSET0] = (1 << l);
}

int main(void)
{
    // create pointer to the gpioregs;
    gpioregs = (unsigned int*)GPIOREGS;
 
    // set gpio22 as output
    gpioregs[GPFSEL2] |= (1 << 6);
 
    while(1)
    {
        blink(500000,22);
    }
    return 0;
}