/*

rgb led - blink a led in red green and blue

gpio 25 is red
gpio 8 is green
gpio 7 is blue

*/


#define GPIOREGS       0x20200000
 
#define GPFSEL0    0
#define GPFSEL1    1
#define GPFSEL2    2
#define GPFSEL3    3
#define GPFSEL4    4
#define GPFSEL5    5
// 6 is reserved
#define GPSET0     7
#define GPSET1     8
// 9 is reserved
#define GPCLR0     10
#define GPCLR1     11

#define RED 25
#define GREEN 8
#define BLUE 7
 
#define HIGH  1
#define LOW 0

volatile unsigned int* gpioregs;
 
void delay(unsigned int t) {
    static volatile unsigned int time;
    for (time = 0; time < t; time++);
}
 
void blink(unsigned int t, unsigned int l) {
    delay(t);
    gpioregs[GPSET0] = (1 << l);
    delay(t);
    gpioregs[GPCLR0] = (1 << l);
}

void setLow(){
    gpioregs[GPCLR0] = (1 << RED);
    gpioregs[GPCLR0] = (1 << GREEN);
    gpioregs[GPCLR0] = (1 << BLUE);
}

void setRGB(int r, int g, int b) {
    setLow();

    if( r == HIGH ){
        gpioregs[GPSET0] = (1 << RED);
    }
    if( g == HIGH ){
        gpioregs[GPSET0] = (1 << GREEN);
    }
    if( b == HIGH ){
        gpioregs[GPSET0] = (1 << BLUE);
    }
}

int main(void)
{
    // create pointer to the gpioregs;
    gpioregs = (unsigned int*)GPIOREGS;
 
    // set led pins as output
    // gpio 25
    gpioregs[GPFSEL2] |= (1 << 15);
    // gpio 8
    gpioregs[GPFSEL0] |= (1 << 24);
    // gpio 7
    gpioregs[GPFSEL0] |= (1 << 21);

    // set all rgb leds to low
    setLow();

    delay(5000000);
    
    blink(2000000,RED);
    blink(2000000,GREEN);
    blink(2000000,BLUE);

    delay(5000000);

    while(1)
    {
        // lets show some color
        setRGB(1,1,1);
        delay(5000000);

        setRGB(0,0,0);
        delay(5000000);

        setRGB(1,0,0);
        delay(5000000);

        setRGB(0,1,0);
        delay(5000000);

        setRGB(0,0,1);
        delay(5000000);

        setRGB(1,1,0);
        delay(5000000);

        setRGB(0,1,1);
        delay(5000000);

        setRGB(1,0,1);
        delay(5000000);

        //blink(500000,22);
    }
    return 0;
}