//  currently written for raspb1....

// https://ictronic.wordpress.com/2016/07/02/interfacing-16x2-lcd-with-arduino-without-potentiometer/
// LCD PIN4 (RS) to R PIN8 (GPIO18)
// LCD PIN6 (EN) to R PIN10 (GPIO23)
// LCD PIN11 (D4) to R PIN12 (GPIO24)
// LCD PIN12 (D5) to R PIN16 (GPIO25)
// LCD PIN13 (D6) to R PIN18 (GPIO8)
// LCD PIN14 (D7) to R PIN22 (GPIO7)

// set register 0 for pin 0 - 31
// set register 1 for pin 31 - 53

#define LCDRS 18
#define LCDEN 23
#define LCDD4 24
#define LCDD5 25
#define LCDD6 8
#define LCDD7 7

//https://www.raspberrypi.org/app/uploads/2012/02/BCM2835-ARM-Peripherals.pdf - register view pahe 90
#define GPIOREGS       0x20200000UL
 
#define GPFSEL0    0
#define GPFSEL1    1
#define GPFSEL2    2
#define GPFSEL3    3
#define GPFSEL4    4
#define GPFSEL5    5
// 6 is reserved
// set0 for pins0-31
// set1 for pins32-53
#define GPSET0     7
#define GPSET1     8
// 9 is reserved
// clr0 for pins0-31
// crl1 for pins32-53
#define GPCLR0     10
#define GPCLR1     11

volatile unsigned int* gpioregs;
 
void delay(unsigned int t) {
    static volatile unsigned int time;
    for (time = 0; time < t; time++);
}
 
#define HIGH  1
#define LOW 0

// send a pulse to the control line lcd pin 6
void pulse() {
    delay(5000);
    gpioregs[GPSET0] = (1 << LCDEN);
    delay(5000);
    gpioregs[GPCLR0] = (1 << LCDEN);
    delay(5000);
}


void clearData() {
    gpioregs[GPCLR0] = (1 << LCDRS);
    gpioregs[GPCLR0] = (1 << LCDD7);
    gpioregs[GPCLR0] = (1 << LCDD6);
    gpioregs[GPCLR0] = (1 << LCDD5);
    gpioregs[GPCLR0] = (1 << LCDD4);
}

void sendData(int rs, int d7, int d6, int d5, int d4) {

    // first we make sure all pins are set to low
    clearData();

    if( rs == HIGH ){
        gpioregs[GPSET0] = (1 << LCDRS);
    }
    if( d7 == HIGH ){
        gpioregs[GPSET0] = (1 << LCDD7);
    }
    if( d6 == HIGH){
        gpioregs[GPSET0] = (1 << LCDD6);
    }
    if( d5 == HIGH ){
        gpioregs[GPSET0] = (1 << LCDD5);
    }
    if( d4 == HIGH ){
        gpioregs[GPSET0] = (1 << LCDD4);
    }
}

// test function
void sendLed(int s) {
    if( s == HIGH) {
        gpioregs[GPSET0] = (1 << 2);
    } else {
        gpioregs[GPCLR0] = (1 << 2);
    }
}

int main(void)
{

    delay(1500000); // wait

    // create pointer to the gpioregs;
    gpioregs = (unsigned int*)GPIOREGS;
 
    // set gpios for lcd to output
    // gpio 18 is in register one bits 24-26
    gpioregs[GPFSEL1] |= (1 << 24);
    // gpio 23 is in register two bits 9-11
    // gpio 24 is in register two bits 12-14
    // gpio 25 is in register two bits 15-17
    gpioregs[GPFSEL2] |= (1 << 9);
    gpioregs[GPFSEL2] |= (1 << 12);
    gpioregs[GPFSEL2] |= (1 << 15);
    // gpio 8 is in register 0 bits 24-26
    // gpio 7 is in register 0 bits 21-23
    gpioregs[GPFSEL0] |= (1 << 21);
    gpioregs[GPFSEL0] |= (1 << 24);    

    // // clear everything
    clearData();

    // lets try to get an H correspondign to the manual page 42
    // sendData(0,0,0,1,1);
    // pulse();
    // sendData(0,0,0,1,1);
    // pulse();
    // sendData(0,0,0,1,1);
    // pulse();
    // sendData(0,0,0,1,0);
    // pulse();
    // sendData(0,1,1,1,0);
    // pulse();
    // sendData(1,0,1,0,0);
    // pulse();
    // sendData(1,1,0,0,0);

    sendData(0,0,0,0,1);


    // // // http://www.site2241.net/november2014.htm#lcdPart1



    while(1)
    {
    //         delay(500000);
    //         sendData(1,0,0,0,0);
    //         delay(500000);
    //         sendData(0,1,0,0,0);
    //         delay(500000);
    //         sendData(0,0,1,0,0);
    //         delay(500000);
    //         sendData(0,0,0,1,0);
    //         delay(500000);
    //         sendData(0,0,0,0,1);
    //         delay(500000);
    //         sendData(1,1,1,1,1);
    }
    return 0;
}