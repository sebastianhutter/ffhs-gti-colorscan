#include "definitions.h"

/*
    GENERAL
*/

void delay(unsigned int t) {
    static volatile unsigned int time;
    for (time = 0; time < t; time++);
}

/*
    GPIO
*/

void gpio_in(unsigned int gpio) {
    INP_GPIO(gpio);
}

void gpio_out(unsigned int gpio) {
    INP_GPIO(gpio);
    OUT_GPIO(gpio);
}

void gpio_blink(unsigned int gpio, unsigned int sleep) {
    HIGH_GPIO(gpio);
    delay(sleep);
    LOW_GPIO(gpio);
    delay(sleep);
}

/*
    BUSY LED
*/

void busy_init() {
    gpio_out(BUSYLED);
}

void busy_blink() {
    gpio_blink(BUSYLED,BUSYLED_TIME);
}

/*
    LCD
*/

void lcd_pulse() {
    delay(5000);
    HIGH_GPIO(LCDEN);
    delay(5000);
    LOW_GPIO(LCDEN);
    delay(5000);  
}

void lcd_send(int *data) {
    int PIN = 0;
    for (int i = 0; i < 9; i++)
    {
        // set the correct pin value
        switch(i) {
            case 0:
                PIN = LCDRS;
                break;
            case 1:
                PIN = LCDD7;
                break;
            case 2:
                PIN = LCDD6;
                break;
            case 3:
                PIN = LCDD5;
                break;
            case 4:
                PIN = LCDD4;
                break;
            case 5:
                PIN = LCDD3;
                break;
            case 6:
                PIN = LCDD2;
                break;
            case 7:
                PIN = LCDD1;
                break;
            case 8:
                PIN = LCDD0;
                break;
        }

        // depending if the data value is high or 
        // low we set the PIN status
        if (data[i] == 1) {
            HIGH_GPIO(PIN);
        } else {
            LOW_GPIO(PIN);
        }
    }
    lcd_pulse();
}

void lcd_init() {
    // set all pins to output
    gpio_out(LCDRS);
    gpio_out(LCDEN);
    gpio_out(LCDD0);
    gpio_out(LCDD1);
    gpio_out(LCDD2);
    gpio_out(LCDD3);
    gpio_out(LCDD4);
    gpio_out(LCDD5);
    gpio_out(LCDD6);
    gpio_out(LCDD7);

    // clear all data pins
    LOW_GPIO(LCDRS);
    LOW_GPIO(LCDD0);
    LOW_GPIO(LCDD1);
    LOW_GPIO(LCDD2);
    LOW_GPIO(LCDD3);
    LOW_GPIO(LCDD4);
    LOW_GPIO(LCDD5);
    LOW_GPIO(LCDD6);
    LOW_GPIO(LCDD7);

    // make sure EN is low too
    LOW_GPIO(LCDEN);

    // the the display function
    lcd_send( (int*)LCD_DISPLAY_FUNCTION );

    // turn the display on and set the cursor to blinking
    lcd_send( (int*)LCD_TURN_DISPLAY );

    // set the entry mode
    lcd_send( (int*)LCD_ENTRY_MODE );
}

// send the specified character
void lcd_sendChar(char c) {
    
    // loop over the character struct and try to find the letter specified
    for (int i = 0; i < LCD_CHARS_SIZE; i++)
    {
        // if the specified letter is found send the data
        // to the display
        if (LCD_CHARS[i].character == c) {
            lcd_send( (int*)LCD_CHARS[i].data );
            return;
        }
    }
    // if no character was send we can assume the character is unknown
    // lets send our unknown character symbol
    lcd_send( (int*)LCD_CHARACTER_NOT_FOUND );
}

// iterate trough the given sentence and send the single chars to the lcd
void lcd_sendString(char *sentence) {
    // https://www.reddit.com/r/C_Programming/comments/2cuf58/iterate_through_a_const_char/
    // https://stackoverflow.com/questions/8831323/find-length-of-string-in-c-without-using-strlen
    int c = 0;
    while (sentence[c] != '\0') {
        lcd_sendChar(sentence[c]);
        c++;
    }
}

// clear the display
void lcd_clearDisplay() {
    // https://stackoverflow.com/questions/5010541/c-pass-array-created-in-the-function-call-line
    lcd_send( (int*)LCD_CLEAR_DISPLAY );
}

// shift cursor left or right
void lcd_shiftCursor(char direction) {
    if ( direction == 'r' ) {
        lcd_send( (int*)LCD_SHIFT_CURSOR_RIGHT );
    } else {
        lcd_send( (int*)LCD_SHIFT_CURSOR_LEFT );
    }
}

// set the cursor position
// line can be 0 or 1
// position can be 0 to 15
void lcd_setCursor(int line, int position) {

    // to make the function easier to read we
    // define the positions of the different 
    // bits in the array as integers
    int rs,d7,d6,d5,d4,d3,d2,d1,d0;
    rs = 0;
    d7 = 1;
    d6 = 2;
    d5 = 3;
    d4 = 4;
    d3 = 5;
    d2 = 6;
    d1 = 7;
    d0 = 8;

    // reset the position array
    LCD_CURSOR_POSITION[rs] = 0;
    LCD_CURSOR_POSITION[d7] = 0;
    LCD_CURSOR_POSITION[d6] = 0;
    LCD_CURSOR_POSITION[d5] = 0;
    LCD_CURSOR_POSITION[d4] = 0;
    LCD_CURSOR_POSITION[d3] = 0;
    LCD_CURSOR_POSITION[d2] = 0;
    LCD_CURSOR_POSITION[d1] = 0;
    LCD_CURSOR_POSITION[d0] = 0;

    // set the line to the first or second line
    // the line number is represented by the lines d7 to d4:
    //       rs   d7 d6 d5 d4
    // line1  0   1  0  0  0  
    // line2  0   1  1  0  0
    if ( line == 0 ) {
        LCD_CURSOR_POSITION[d7] = 1;
        LCD_CURSOR_POSITION[d6] = 0;
    } else {
        LCD_CURSOR_POSITION[d7] = 1;
        LCD_CURSOR_POSITION[d6] = 1;

    }

    // lets set the position
    // the position is represented by d3 to d0
    //         (8) (4) (2) (1)
    //          d3  d2  d1  d0  
    // pos  0   0   0   0   0
    // pos  1   0   0   0   1
    // pos  2   0   0   1   0
    // pos  3   0   0   1   1
    // pos  4   0   1   0   0
    // pos  5   0   1   0   1
    // pos  6   0   1   1   0
    // pos  7   0   1   1   1
    // pos  8   1   0   0   0
    // pos  9   1   0   0   1
    // pos 10   1   0   1   0
    // pos 11   1   0   1   1
    // pos 12   1   1   0   0
    // pos 13   1   1   0   1
    // pos 14   1   1   1   0
    // pos 15   1   1   1   1

    // if position is bigger then 15 we reset it to 15
    if (position > 15) { position = 15; }

    // if the position is 8 or higher we set d3 to high
    // if the position - 8 is 4 or higher we need to set d2 to high
    // if the position - 8 - 4 is 2 or higher we need to set d1 to high
    // if the position - 8 - 4 - 2 is 1 or higher we need to set d0 to high 
    if (position >= 8) {
        LCD_CURSOR_POSITION[d3] = 1;
        position = position - 8;
    }
    if ( position >= 4) {
        LCD_CURSOR_POSITION[d2] = 1;
        position = position - 4;
    }
    if ( position >= 2) {
        LCD_CURSOR_POSITION[d1] = 1;
        position = position - 2;
    }
    if ( position >= 1) {
        LCD_CURSOR_POSITION[d0] = 1;
    }

    // lets set the cursor
    lcd_send( (int*)LCD_CURSOR_POSITION );
}

/*
    RGB and PHOTOCELL 
*/

void rgb_init() {
    // set the rgb pins as output
    // for whatever reason i need to add a minimal delay between initalizing the pins
    // that isnt necessary for the lcd pins but without the delay the pins wont
    // be set to output....
    gpio_out(RGB_RED);
    LOW_GPIO(RGB_RED);
    delay(1000);

    gpio_out(RGB_GREEN);
    LOW_GPIO(RGB_GREEN);
    delay(1000);

    gpio_out(RGB_BLUE);
    LOW_GPIO(RGB_BLUE);
    delay(1000);
}

void rgb_setColor(int r, int g, int b) {
    if(r == HIGH) {
        HIGH_GPIO(RGB_RED);
    } else {
        LOW_GPIO(RGB_RED);
    }
    if(g == HIGH) {
        HIGH_GPIO(RGB_GREEN);
    } else {
        LOW_GPIO(RGB_GREEN);
    }
    if(b == HIGH) {
        HIGH_GPIO(RGB_BLUE);
    } else {
        LOW_GPIO(RGB_BLUE);
    }
}

void photo_init() {
    // set the photocell as input
    gpio_in(PHOTO_PIN);
}

void photo_getPin() {
    // get the value from the photo pin and return the float
    GPIO_READ(PHOTO_PIN);

    //return value;
}

