/******************************************************************************
    contain global defintions for the rasperry board and the used components
********************************************************************************/

/*
    GENERAL DEFINITIONS   
*/ 

// whats high and whats low
#define HIGH 1
#define LOW 0

/*
    RASPBERRY ADDRESSES
*/

#define BASE       0x3F000000
#define GPIO_BASE (BASE + 0x200000)
//#define BSC0_BASE (BASE + 0x205000)
#define BSC1_BASE (BASE + 0x804000)
//#define BSC2_BASE (BASE + 0x805000)

/*
    GPIO
*/

// set gpio pin as input
// first we get the select register by diving the gpio pin by 10 (we got 10 select registers)
// and then we add the result to the gpio base 
// now we send 000 to the three bits in the register 
#define INP_GPIO(g) *((unsigned int *)GPIO_BASE + ((g)/10)) &= ~(7<<(((g)%10)*3))
// to set as output we need to send 00
#define OUT_GPIO(g) *((unsigned int *)GPIO_BASE + ((g)/10)) |=  (1<<(((g)%10)*3))
// see http://www.pieter-jan.com/node/15 for more details

// macros to set HIGH and LOW a GPIO pin
#define HIGH_GPIO(g) *((unsigned int *)GPIO_BASE + 7) = (1 << g)
#define LOW_GPIO(g) *((unsigned int *)GPIO_BASE + 10) = (1 << g)

// macro to read the level register for the selected pin
#define GPIO_READ(g)  *((unsigned int *)GPIO_BASE + 13) &= (1<<(g))

/*
    I2C and TCS347725
*/
// 2018-03-25: http://www.susa.net/wordpress/2012/06/raspberry-pi-pcf8563-real-time-clock-rtc/

#define BSC0_C        *(BSC1_BASE + 0x00)
#define BSC0_S        *(BSC1_BASE + 0x01)
#define BSC0_DLEN    *(BSC1_BASE + 0x02)
#define BSC0_A        *(BSC1_BASE + 0x03)
#define BSC0_FIFO    *(BSC1_BASE + 0x04)

#define BSC_C_I2CEN    (1 << 15)
#define BSC_C_INTR    (1 << 10)
#define BSC_C_INTT    (1 << 9)
#define BSC_C_INTD    (1 << 8)
#define BSC_C_ST    (1 << 7)
#define BSC_C_CLEAR    (1 << 4)
#define BSC_C_READ    1

#define START_READ    BSC_C_I2CEN|BSC_C_ST|BSC_C_CLEAR|BSC_C_READ
#define START_WRITE    BSC_C_I2CEN|BSC_C_ST

#define BSC_S_CLKT    (1 << 9)
#define BSC_S_ERR    (1 << 8)
#define BSC_S_RXF    (1 << 7)
#define BSC_S_TXE    (1 << 6)
#define BSC_S_RXD    (1 << 5)
#define BSC_S_TXD    (1 << 4)
#define BSC_S_RXR    (1 << 3)
#define BSC_S_TXW    (1 << 2)
#define BSC_S_DONE    (1 << 1)
#define BSC_S_TA    1

#define CLEAR_STATUS    BSC_S_CLKT|BSC_S_ERR|BSC_S_DONE

#define PAGESIZE 4096
#define BLOCK_SIZE 4096




/*
    TCS347725
*/
// 2018-03-25: https://cdn-shop.adafruit.com/datasheets/TCS34725.pdf

// hardcoded address for the i2c interface
#define TCS_ADDRESS 0x39      

// register addresses
//#define TCS_REGISTER_COMMAND 0x00 // the command register does not have an address set ....
#define TCS_REGISTER_ENABLE  0x00  // enable register to power on / power off device
#define TCS_REGISTER_ATIME   0x01  // RGBC time register *NOT USED*
#define TCS_REGISTER_WTIME   0x03  // Wait time *NOT USED*
#define TCS_REGISTER_AILTL   0x04  // Clear interrupt low threshold low byte *NOT USED*
#define TCS_REGISTER_AILTH   0x05  // Clear interrupt low threshold high byte 
#define TCS_REGISTER_AIHTL   0x06  // Clear interrupt high threshold low byte
#define TCS_REGISTER_AIHTH   0x07  // Clear interrupt high threshold high byte
#define TCS_REGISTER_PERS    0x0C  // Interrupt persistence filter
#define TCS_REGISTER_CONFIG  0x0D  // Configuration
#define TCS_REGISTER_CONTROL 0x0F  // Control
#define TCS_REGISTER_ID      0x12  // Device ID
#define TCS_REGISTER_STATUS  0x13  // Device status
#define TCS_REGISTER_CDATAL  0x14  // Clear data low byte
#define TCS_REGISTER_CDATAH  0x15  // Clear data high byte
#define TCS_REGISTER_RDATAL  0x16  // Red data low byte
#define TCS_REGISTER_RDATAH  0x17  // Red data high byte
#define TCS_REGISTER_GDATAL  0x18  // Green data low byte
#define TCS_REGISTER_GDATAH  0x19  // Green data high byte
#define TCS_REGISTER_BDATAL  0x1A  // Blue data low byte
#define TCS_REGISTER_BDATAH  0x1B  // Blue data high byte


// enable register values
#define TCS_ENABLE_POWERON = {0, 0, 0, 0, 0, 0, 0, 0}

/*
    BUSY LED PIN
*/

#define BUSYLED 26
// delay time for the blink
#define BUSYLED_TIME 50000


/*
    RGB LED AND PHOTOCELL PINS
*/

// set the pins for the rgb led
#define RGB_RED 6
#define RGB_GREEN 13
#define RGB_BLUE 19

// set the photocell pin
#define PHOTO_PIN 5

/*
    LCD PINS AND INSTRUCTIONS
*/

// lcd GPIO Pins
#define LCDRS 18
#define LCDEN 23

#define LCDD0 24
#define LCDD1 25
#define LCDD2 8
#define LCDD3 7
#define LCDD4 1
#define LCDD5 12
#define LCDD6 16
#define LCDD7 20

// lcd letters
struct LCD_CHAR {
    // character to represent
    char character;
    // rs + data (1bit rs + 8bit data)
    int data[9];
};

// lets define different characters for the lcd screen
// see page 18 in http://www.site2241.net/lcm1602c/HD44780.pdf
const int LCD_CHARS_SIZE = 71;
const struct LCD_CHAR LCD_CHARS[71] = {
    //A-Z
    {'A', {1, 0,1,0,0, 0,0,0,1}},
    {'B', {1, 0,1,0,0, 0,0,1,0}},
    {'C', {1, 0,1,0,0, 0,0,1,1}},
    {'D', {1, 0,1,0,0, 0,1,0,0}},
    {'E', {1, 0,1,0,0, 0,1,0,1}},
    {'F', {1, 0,1,0,0, 0,1,1,0}},
    {'G', {1, 0,1,0,0, 0,1,1,1}},
    {'H', {1, 0,1,0,0, 1,0,0,0}},
    {'I', {1, 0,1,0,0, 1,0,0,1}},
    {'J', {1, 0,1,0,0, 1,0,1,0}},
    {'K', {1, 0,1,0,0, 1,0,1,1}},
    {'L', {1, 0,1,0,0, 1,1,0,0}},
    {'M', {1, 0,1,0,0, 1,1,0,1}},
    {'N', {1, 0,1,0,0, 1,1,1,0}},
    {'O', {1, 0,1,0,0, 1,1,1,1}},
    {'P', {1, 0,1,0,1, 0,0,0,0}},
    {'Q', {1, 0,1,0,1, 0,0,0,1}},
    {'R', {1, 0,1,0,1, 0,0,1,0}},
    {'S', {1, 0,1,0,1, 0,0,1,1}},
    {'T', {1, 0,1,0,1, 0,1,0,0}},
    {'U', {1, 0,1,0,1, 0,1,0,1}},
    {'V', {1, 0,1,0,1, 0,1,1,0}},
    {'W', {1, 0,1,0,1, 0,1,1,1}},
    {'X', {1, 0,1,0,1, 1,0,0,0}},
    {'Y', {1, 0,1,0,1, 1,0,0,1}},
    {'Z', {1, 0,1,0,1, 1,0,1,0}},
    //a-z
    {'a', {1, 0,1,1,0, 0,0,0,1}},
    {'b', {1, 0,1,1,0, 0,0,1,0}},
    {'c', {1, 0,1,1,0, 0,0,1,1}},
    {'d', {1, 0,1,1,0, 0,1,0,0}},
    {'e', {1, 0,1,1,0, 0,1,0,1}},
    {'f', {1, 0,1,1,0, 0,1,1,0}},
    {'g', {1, 0,1,1,0, 0,1,1,1}},
    {'h', {1, 0,1,1,0, 1,0,0,0}},
    {'i', {1, 0,1,1,0, 1,0,0,1}},
    {'j', {1, 0,1,1,0, 1,0,1,0}},
    {'k', {1, 0,1,1,0, 1,0,1,1}},
    {'l', {1, 0,1,1,0, 1,1,0,0}},
    {'m', {1, 0,1,1,0, 1,1,0,1}},
    {'n', {1, 0,1,1,0, 1,1,1,0}},
    {'o', {1, 0,1,1,0, 1,1,1,1}},
    {'p', {1, 0,1,1,1, 0,0,0,0}},
    {'q', {1, 0,1,1,1, 0,0,0,1}},
    {'r', {1, 0,1,1,1, 0,0,1,0}},
    {'s', {1, 0,1,1,1, 0,0,1,1}},
    {'t', {1, 0,1,1,1, 0,1,0,0}},
    {'u', {1, 0,1,1,1, 0,1,0,1}},
    {'v', {1, 0,1,1,1, 0,1,1,0}},
    {'w', {1, 0,1,1,1, 0,1,1,1}},
    {'x', {1, 0,1,1,1, 1,0,0,0}},
    {'y', {1, 0,1,1,1, 1,0,0,1}},
    {'z', {1, 0,1,1,1, 1,0,1,0}},
    // 0-9
    {'0', {1, 0,0,1,1, 0,0,0,0}},
    {'1', {1, 0,0,1,1, 0,0,0,1}},
    {'2', {1, 0,0,1,1, 0,0,1,0}},
    {'3', {1, 0,0,1,1, 0,0,1,1}},
    {'4', {1, 0,0,1,1, 0,1,0,0}},
    {'5', {1, 0,0,1,1, 0,1,0,1}},
    {'6', {1, 0,0,1,1, 0,1,1,0}},
    {'7', {1, 0,0,1,1, 0,1,1,1}},
    {'8', {1, 0,0,1,1, 1,0,0,0}},
    {'9', {1, 0,0,1,1, 1,0,0,1}},
    // = , . : ; \s - ?
    {' ', {1, 0,0,1,0, 0,0,1,0}},
    {'-', {1, 0,0,1,0, 1,1,0,1}},
    {'.', {1, 0,0,1,0, 1,1,1,0}},
    {',', {1, 0,0,1,0, 1,1,0,0}},
    {':', {1, 0,0,1,1, 1,0,1,0}},
    {';', {1, 0,0,1,1, 1,0,1,1}},
    {'=', {1, 0,0,1,1, 1,1,0,1}},
    {'?', {1, 0,0,1,1, 1,1,1,1}},
    {'!', {1, 0,0,1,0, 0,0,0,1}}
};
// character not found symbol (*)
const int LCD_CHARACTER_NOT_FOUND[9] = {1, 0,0,1,0, 1,0,1,0};
// reset display and set cursor position to 0
const int LCD_CLEAR_DISPLAY[9] =       {0, 0,0,0,0, 0,0,0,1};
// set the display mode to 8bit, 2lines, 5*8 font
const int LCD_DISPLAY_FUNCTION[9] =    {0, 0,0,1,1, 1,0,0,0};
// turn on the display and set the cursor to blinking
const int LCD_TURN_DISPLAY[9] =        {0, 0,0,0,0, 1,1,1,1};
// set the entry mode with incrementing curser and display not
// shifting
const int LCD_ENTRY_MODE[9] =          {0, 0,0,0,0, 0,1,1,0};
// shif cursor left or right
const int LCD_SHIFT_CURSOR_RIGHT[9] =  {0, 0,0,0,1, 0,1,0,0};
const int LCD_SHIFT_CURSOR_LEFT[9] =   {0, 0,0,0,1, 0,0,0,0};

// create global array to hold cursor position
// if i define the array inside a function I get the error that memset is not defiend....
int LCD_CURSOR_POSITION[9] = {0, 0,0,0,0, 0,0,0,0};
