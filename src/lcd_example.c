/*
    https://ictronic.wordpress.com/2016/07/02/interfacing-16x2-lcd-with-arduino-without-potentiometer/
    LCD PIN01 (VSS) to GRND
    LCD PIN02 (VDD) to 5VOL
    LCD PIN03 (VEE) to GRND
    // RS = register select; send data or send instructions
    LCD PIN04 (RS ) to PIN08 (GPIO18) 
    LCD PIN05 (RW ) to GRND
    // EN = enable; instruct to act on received data or instructions
    LCD PIN06 (EN ) to PIN10 (GPIO23) 
    // 8 data pins DB0 to DB7 used to send instructions or data
    LCD PIN07 (DB0) to PIN18 (GPIO24)
    LCD PIN08 (DB1) to PIN22 (GPIO25)
    LCD PIN09 (DB2) to PIN24 (GPIO08)
    LCD PIN10 (DB3) to PIN26 (GPIO07)
    LCD PIN11 (DB4) to R PIN28 (GPIO01)
    LCD PIN12 (DB5) to R PIN32 (GPIO12)
    LCD PIN13 (DB6) to R PIN36 (GPIO16)
    LCD PIN14 (DB7) to R PIN38 (GPIO20)
    // pins for backlight
    LCD PIN15 (+PWR) to 5VOL (with 220ohm)
    LCD PIN16 (-PWR) to GRND
*/

#include "functions.h"


int application(void)
{

    // initialize the busy led
    busy_init();

    // initialize the lcd
    lcd_init();

    while(1)
    {   
        // lets blink the busy led so we know we are at the start of the while loop
        busy_blink();
        // write hello world to the display
        lcd_clearDisplay();
        lcd_setCursor(0,2);
        lcd_sendString("Hello,");
        lcd_setCursor(1,2);
        lcd_sendString("World!");
        delay(500000);
    }
    return 0;
}
